<?php
$hostname = "localhost";
$username = "root";
$password = "admin";
$database = "stock";
$row_limit = 8;
$sgbd = 'mysql'; // mysql, pgsql

// conexion to mysql
try {
    $pdo = new PDO($sgbd.":host=$hostname;dbname=$database", $username, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "conexion establecida";

} catch (PDOException $err) {
    echo"fallo en la conexion a la base de datos";
    die("Error! " . $err->getMessage());
}
